# EXODUS #

This is the BitBucket repository for our Exodus-application. The goal is to make it easy for refugees (and other people who'd like to move cross border) to find a country that suits him/her the best.

### What does the application do? ###

* It asks you 16 questions
* It will give a top 5 of countries which suit you the best
* It will show you some basic information about the country and a map

### What technologies are being used? ###

* HTML&CSS
* JavaScript

### What 3rd party libraries, plugins and technologies were used? ###

* [JQuery](https://jquery.com/)
* [Crossroads.js](https://millermedeiros.github.io/crossroads.js/)
* [js-signals](https://millermedeiros.github.io/js-signals/)
* [Hasher](https://github.com/millermedeiros/hasher/)
* [handlebars](http://handlebarsjs.com/)
* [lodash](https://lodash.com/)
* [normalize.css](https://necolas.github.io/normalize.css/)
* [Font Awesome](https://fortawesome.github.io/Font-Awesome/)
* [Weather Icons](http://erikflowers.github.io/weather-icons/)

## What automation tools were used?

* [Bower](http://bower.io/)
* [Grunt](http://gruntjs.com/)

### Who made this possible? ###

* Bram De Backer
* Zeger Caes
* Feline Dekeyser