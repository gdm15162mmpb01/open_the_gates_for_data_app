module.exports = function(grunt){
        
    // Project configuration.
    grunt.initConfig({
    htmlhint: {
        build: {
            options: {
                'tag-pair': true,
                'tagname-lowercase': true,
                'attr-lowercase': true,
                'attr-value-double-quotes': true,
                'doctype-first': true,
                'spec-char-escape': true,
                'id-unique': true,
                'head-script-disabled': true,
                'style-disabled': true
            },
            src: ['app/index.html','app/404.html']
        }
    },
    concat: {
        options: {
            separator: '\n\n',
        },
        dist: {
            src: ['app/styles/css/assets/grid.css','app/styles/css/assets/loader.css','app/styles/css/assets/router.css','app/styles/css/assets/common.css'],
            dest: 'app/styles/css/app.css',
        },
    },
    uglify: {
        build: {
            files: {
                'app/scripts/js/app.min.js': ['app/scripts/js/assets/google_maps.js','app/scripts/js/assets/country.js', 'app/scripts/js/assets/utils.js', 'app/scripts/js/assets/main.js']
            }
        }
    },
    cssmin: {
        target: {
            files: [{
            expand: true,
            cwd: 'app/styles/css',
            src: ['app.css'],
            dest: 'app/styles/css',
            ext: '.min.css'
            },
            ]
        }
    },
    watch: {
        html: {
            files: ['app/index.html'],
            tasks: ['htmlhint']
        },
        js: {
            files: ['app/scripts/js/*.js'],
            tasks: ['uglify']
        },
        css: {
            files: ['app/styles/css/assets/*.css'],
            tasks: ['concat', 'cssmin']
        }
    }
    });
    
    require('load-grunt-tasks')(grunt);
    grunt.registerTask('default', ['htmlhint', 'concat', 'cssmin', 'uglify']);

};
